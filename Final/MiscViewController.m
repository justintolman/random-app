//
//  MiscViewController.m
//  Final
//
//  Created by Justin Tolman on 4/20/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "MiscViewController.h"
#import "RandomNunberFactory.h"

@interface MiscViewController ()
@property (strong, nonatomic) IBOutlet UILabel *yesNoOutput;
@property (strong, nonatomic) IBOutlet UILabel *numberOutput;
@property (strong, nonatomic) IBOutlet UILabel *angleOutput;
@property (strong, nonatomic) IBOutlet UILabel *dayNameOutput;
@property (strong, nonatomic) IBOutlet UILabel *firstNameOutput;
@property (strong, nonatomic) IBOutlet UILabel *lastNameOutput;
@property (strong, nonatomic) IBOutlet UILabel *latitudeOutput;
@property (strong, nonatomic) IBOutlet UILabel *longitudeOutput;
@property (strong, nonatomic) IBOutlet UILabel *monthOutput;
@property (strong, nonatomic) IBOutlet UILabel *dayOutput;
@property (strong, nonatomic) IBOutlet UILabel *yearoutput;
@property (strong, nonatomic) IBOutlet UILabel *hourOutput;
@property (strong, nonatomic) IBOutlet UILabel *minuteOutput;
@property (strong, nonatomic) IBOutlet UILabel *secondOutput;
@property (strong, nonatomic) IBOutlet UILabel *amPmOutput;
@property (strong, nonatomic) IBOutlet UILabel *lottoOutput0;
@property (strong, nonatomic) IBOutlet UILabel *lottoOutput1;
@property (strong, nonatomic) IBOutlet UILabel *lottoOutput2;
@property (strong, nonatomic) IBOutlet UILabel *lottoOutput3;
@property (strong, nonatomic) IBOutlet UILabel *lottoOutput4;
@property (strong, nonatomic) IBOutlet UILabel *lottoOutput5;
@property (strong, nonatomic) IBOutlet UILabel *predictionOutput;
@end

@implementation MiscViewController{
    RandomNunberFactory *_random;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _random=[RandomNunberFactory sharedRandomNunberFactory];
    [self refresh];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)refresh{
    _yesNoOutput.text=[_random aOrB:@"Yes" optB:@"No"];
    _numberOutput.text=[NSString stringWithFormat:@"%lu",(long)[_random generateNumber:0 randMax:99]];
    _angleOutput.text=[NSString stringWithFormat:@"%ld°",(long)[_random generateNumber:0 randMax:359]];
    _dayNameOutput.text=[_random stringFromArray:[NSArray arrayWithObjects: @"Sun", @"Mon", @"Tues", @"Wed", @"Thurs", @"Fri", @"Sat", nil]];
    NSUInteger month=[_random generateNumber:1 randMax:12];
    NSUInteger days;
    _monthOutput.text=[NSString stringWithFormat:@"%02lu",(long)month];
    switch (month) {
        case 2:
            days=28;
            break;
        case 4:
            days=30;
            break;
        case 6:
            days=30;
            break;
        case 9:
            days=30;
            break;
        case 11:
            days=30;
            break;
            
        default:
            days=31;
            break;
    }
    _dayOutput.text=[NSString stringWithFormat:@"%02lu",(long)[_random generateNumber:1 randMax:days]];
    _yearoutput.text=[NSString stringWithFormat:@"%02lu",(long)[_random generateNumber:0 randMax:99]];
    _hourOutput.text=[NSString stringWithFormat:@"%02lu",(long)[_random generateNumber:1 randMax:12]];
    _minuteOutput.text=[NSString stringWithFormat:@"%02lu",(long)[_random generateNumber:0 randMax:59]];
    _secondOutput.text=[NSString stringWithFormat:@"%02lu",(long)[_random generateNumber:0 randMax:59]];
    _amPmOutput.text=[_random aOrB:@"AM" optB:@"PM"];
    _lottoOutput0.text=[NSString stringWithFormat:@"%02lu",(long)[_random generateNumber:1 randMax:59]];
    _lottoOutput1.text=[NSString stringWithFormat:@"%02lu",(long)[_random generateNumber:1 randMax:59]];
    _lottoOutput2.text=[NSString stringWithFormat:@"%02lu",(long)[_random generateNumber:1 randMax:59]];
    _lottoOutput3.text=[NSString stringWithFormat:@"%02lu",(long)[_random generateNumber:1 randMax:59]];
    _lottoOutput4.text=[NSString stringWithFormat:@"%02lu",(long)[_random generateNumber:1 randMax:59]];
    _lottoOutput5.text=[NSString stringWithFormat:@"%02lu",(long)[_random generateNumber:1 randMax:59]];
}
- (IBAction)buttonPressed:(id)sender {
    [self refresh];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
