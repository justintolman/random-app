//
//  SelectorViewController.m
//  Final
//
//  Created by Justin Tolman on 4/19/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "SelectorViewController.h"

@interface SelectorViewController (){
    NSMutableArray *_labels;
    NSMutableArray *_spinnerList;
}
@property (strong, nonatomic) IBOutlet UIView *spinnerView;@end

@implementation SelectorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)spinSpinnerUp:(id)sender {
}

- (IBAction)spinSpinnerDown:(id)sender {
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    for (int i=0;i<15;i++){
        UILabel *label =  [[UILabel alloc] initWithFrame:CGRectMake(-_spinnerView.layer.frame.size.width*1.2, _spinnerView.layer.frame.size.height/2, _spinnerView.layer.frame.size.width*.8, 60)];
        label.text =[NSString stringWithFormat: @"%d",i]; //etc...
        label.textAlignment = NSTextAlignmentCenter;
        label.layer.anchorPoint = CGPointMake(-1,0.5);
        label.transform = CGAffineTransformMakeRotation((i-7)*M_PI_4/8);
        [_labels addObject:label];
        [_spinnerView addSubview:label];
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
