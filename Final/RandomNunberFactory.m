//
//  RandomNunberFactory.m
//  Final
//
//  Created by Justin Tolman on 4/13/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <sqlite3.h>
#import "RandomNunberFactory.h"
@implementation RandomNunberFactory{
    sqlite3 *_database;
    NSMutableArray *_MaleNames;
    NSMutableArray *_FemaleNames;
    NSMutableArray *_LastNames;
}

+ (id)sharedRandomNunberFactory {
    static RandomNunberFactory *sharedRandomNunberFactory = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedRandomNunberFactory = [[self alloc] init];
    });
    return sharedRandomNunberFactory;
}

-(id)init{
    self=[super init];
    _MaleNames=[self arrayFromTable: @"MaleNames"];
    _FemaleNames=[self arrayFromTable: @"FemaleNames"];
    _LastNames=[self arrayFromTable: @"LastNames"];
    return self;
}

-(NSInteger)generateNumber:(NSInteger) rMin randMax: (NSInteger) rMax {
    return arc4random_uniform((rMax+1)-rMin)+rMin;
}

-(BOOL) randBOOL{
    return[self generateNumber:0 randMax:1];
}

-(NSString*) aOrB:(NSString *)A optB:(NSString *)B {
    BOOL n=[self randBOOL];
    if(n)return A;
    return B;
}

-(NSString*) stringFromArray:(NSArray *)Array {
    switch ([Array count]) {
        case 0:
            return @"";
            break;
            
        case 1:
            return Array[0];
            break;
            
        case 2:
            return [self aOrB:[Array objectAtIndex: 0] optB:[Array objectAtIndex: 1]];
            break;
            
        default:
            return [Array objectAtIndex: [self generateNumber:0 randMax:[Array count]-1]];
            break;
    }
}

-(NSMutableArray*) arrayFromTable:(NSString*) table
{
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    NSString *sqLiteDb = [[NSBundle mainBundle] pathForResource:@"RandomData" ofType:@"sqlite"];
    if(sqlite3_open([sqLiteDb UTF8String], &_database) == SQLITE_OK)
    {
        // Setup the SQL Statement and compile it for faster access
        //SQLIte Statement
        NSString *sqlStatement =[NSString stringWithFormat:@"SELECT List FROM %@", table];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(_database, [sqlStatement UTF8String], -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
               // NSLog([NSString stringWithFormat:@"%s",sqlite3_column_int(compiledStatement, 1)]);
                //[temp addObject:[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_int(compiledStatement, 1)]];
            }
            return temp;
        }
        else
        {
            NSLog(@"No Data Found");
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(_database);
    return temp;
}
@end
