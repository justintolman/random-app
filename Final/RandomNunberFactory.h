//
//  RandomNunberFactory.h
//  Final
//
//  Created by Justin Tolman on 4/13/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RandomNunberFactory : NSObject

-(NSInteger)generateNumber:(NSInteger) rMin randMax: (NSInteger) rMax;

-(BOOL) randBOOL;

-(NSString*) aOrB:(NSString*)A optB:(NSString*)B;

-(NSString*) stringFromArray:(NSArray*)Array;

+ (id)sharedRandomNunberFactory;
@end
