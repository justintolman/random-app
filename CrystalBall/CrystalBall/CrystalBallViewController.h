//
//  CrystalBallViewController.h
//  CrystalBall
//

//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface CrystalBallViewController : UIViewController
@end
