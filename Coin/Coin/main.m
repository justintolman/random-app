//
//  main.m
//  Coin
//
//  Created by Justin Tolman on 4/17/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CoinDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CoinDelegate class]));
    }
}
