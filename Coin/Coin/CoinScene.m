//
//  MyScene.m
//  Final
//
//  Created by Justin Tolman on 4/13/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "CoinScene.h"

@implementation CoinScene{
    SKSpriteNode *_coin;
    BOOL flipEnabled;
    NSMutableArray *_flipFrames;
    RandomNunberFactory *_random;
}

-(id)initWithSize:(CGSize)size {
        if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.backgroundColor = [SKColor blackColor];
        [self setPhysicsBody: [SKPhysicsBody bodyWithEdgeLoopFromRect: self.frame]];
        [self.physicsWorld setGravity: CGVectorMake(0, -10)];
        
        
        SKSpriteNode *floor = [SKSpriteNode spriteNodeWithColor:[SKColor blackColor] size: CGSizeMake(300, 10)];
        floor.position = CGPointMake(size.width/2, 50);
        [floor setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: floor.frame.size]];
        floor.physicsBody.dynamic=NO;
        [self addChild: floor];
        
        _flipFrames=[NSMutableArray array];
        SKTextureAtlas *coinAnimatedAtlas=[SKTextureAtlas atlasNamed:@"coin"];
        int numImages=(int)coinAnimatedAtlas.textureNames.count;
        for(int i=0; i<numImages; i++){
            NSString *textureName=[NSString stringWithFormat:@"coin%d", i];
            SKTexture *temp=[coinAnimatedAtlas textureNamed:textureName];
            [_flipFrames addObject:temp];
        }
        SKTexture *temp = _flipFrames[0];
        _coin=[SKSpriteNode spriteNodeWithTexture:temp];
        _coin.position = CGPointMake(CGRectGetMidX(self.frame), 55);
        [_coin setPhysicsBody: [SKPhysicsBody bodyWithRectangleOfSize: _coin.frame.size]];
        flipEnabled=YES;
        [self addChild:_coin];
        CFURLRef tmp;
        tmp = CFBundleCopyResourceURL(CFBundleGetMainBundle(), (CFStringRef) @"CoinFlip", CFSTR ("wav"), NULL);
        AudioServicesCreateSystemSoundID(tmp, &_coinFlipSound);
        CFRelease(tmp);
        _random=[RandomNunberFactory sharedRandomNunberFactory];
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if(flipEnabled)[self flipCoin];
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

-(void)flipCoin
{
    if(flipEnabled){
        [self disableFlip];
        [NSTimer scheduledTimerWithTimeInterval:1.4 target:self selector:@selector(enableFlip:) userInfo:nil repeats:NO];
        [_coin.physicsBody setVelocity: CGVectorMake(0, 1000.0f)];
        AudioServicesPlaySystemSound(_coinFlipSound);
        [_coin runAction:[SKAction repeatAction:
                      [SKAction animateWithTextures:_flipFrames
                                       timePerFrame:0.01f
                                             resize:YES
                                            restore:YES] count:8] withKey:@"flippingCoin"];
        if([_random randBOOL])_coin.texture=_flipFrames[0];
        else _coin.texture=_flipFrames[8];
    }
    return;
}

-(void) enableFlip:(id)sender{
    flipEnabled=YES;
}

-(void) disableFlip{
    flipEnabled=NO;
}

@end
