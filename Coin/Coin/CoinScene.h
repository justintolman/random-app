//
//  CoinScene.h
//  Final
//

//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import "RandomNunberFactory.h"

@interface CoinScene : SKScene{
	SystemSoundID _coinFlipSound;
}

-(void)flipCoin;

@end
