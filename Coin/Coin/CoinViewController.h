//
//  CoinViewController.h
//  Final
//

//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <CoreMotion/CoreMotion.h>

@interface CoinViewController : UIViewController

@end
